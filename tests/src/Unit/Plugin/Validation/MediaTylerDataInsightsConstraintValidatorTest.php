<?php

declare(strict_types=1);

namespace Drupal\Tests\media_tyler_data_insights\Unit\Plugin\Validation\Constraint;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\media\MediaInterface;
use Drupal\media_tyler_data_insights\Plugin\media\Source\TylerDataInsightsSource;
use Drupal\media_tyler_data_insights\Plugin\Validation\Constraint\MediaTylerDataInsightsConstraint;
use Drupal\media_tyler_data_insights\Plugin\Validation\Constraint\MediaTylerDataInsightsConstraintValidator;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Tests the constraint validation logic.
 *
 * @group media_tyler_data_insights
 */
class MediaTylerDataInsightsConstraintValidatorTest extends UnitTestCase {

  /**
   * The constraint validotor under test.
   *
   * @var \Drupal\media_tyler_data_insights\Plugin\Validation\Constraint\MediaTylerDataInsightsConstraintValidator
   */
  protected MediaTylerDataInsightsConstraintValidator $sut;

  /**
   * The constraint.
   *
   * @var \Drupal\media_tyler_data_insights\Plugin\Validation\Constraint\MediaTylerDataInsightsConstraint
   */
  protected MediaTylerDataInsightsConstraint $constraint;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $configFactory;

  /**
   * The execution context.
   *
   * @var \Symfony\Component\Validator\Context\ExecutionContextInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $context;

  /**
   * The list of allowed hosts for this test.
   */
  protected const ALLOWED_HOSTS = [
    'https://evergreen.data.socrata.com',
    'https://example.org:8080',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $config = $this->createMock(Config::class);
    $config->expects($this->any())
      ->method('get')
      ->with('allowed_hosts')
      ->willReturn(static::ALLOWED_HOSTS);
    $this->configFactory = $this->createMock(ConfigFactoryInterface::class);
    $this->configFactory->expects($this->any())
      ->method('get')
      ->with('media_tyler_data_insights.settings')
      ->willReturn($config);
    $this->context = $this->createMock(ExecutionContextInterface::class);
    $this->constraint = new MediaTylerDataInsightsConstraint([]);

    $this->sut = new MediaTylerDataInsightsConstraintValidator($this->configFactory);
    $this->sut->initialize($this->context);
  }

  /**
   * Tests the validator works for different snippets as expected.
   *
   * @dataProvider validateDataProvider
   */
  public function testValidate(string $snippet, ?string $url, ?string $errorMessageKey) {
    $entity = $this->createMock(MediaInterface::class);
    $source = $this->createMock(TylerDataInsightsSource::class);
    $value = $this->createMock(FieldItemInterface::class);
    $value->expects($this->any())
      ->method('getEntity')
      ->willReturn($entity);
    $entity->expects($this->any())
      ->method('getSource')
      ->willReturn($source);
    $source->expects($this->any())
      ->method('getSourceFieldValue')
      ->with($entity)
      ->willReturn($snippet);

    $this->context->expects($this->exactly($errorMessageKey ? 1 : 0))
      ->method('addViolation');

    if ($errorMessageKey !== NULL) {
      $this->context->expects($this->once())
        ->method('addViolation')
        ->with($this->constraint->{$errorMessageKey});
    }
    $this->sut->validate($value, $this->constraint);
  }

  /**
   * Data provider for :testValidate.
   */
  public function validateDataProvider(): \Generator {
    yield 'invalid snippet not containing iframe' => [
      <<<SNIPPET
      This is my value
    SNIPPET,
      NULL,
      'invalidEmbedCodeMessage',
    ];

    yield 'duplicated snippet with two iframes' => [
      <<<SNIPPET
      <div><iframe width="950px" title="Washington State Median Income by County" height="808px" src="https://evergreen.data.socrata.com/w/yiqu-vird/s4mb-uumi?cur=rYVjbMrWceN&from=root" frameborder="0"scrolling="no"><a href="https://evergreen.data.socrata.com/dataset/Washington-State-Median-Income-by-County/yiqu-vird" title="Washington State Median Income by County" target="_blank">Washington State Median Income by County</a></iframe><p><a href="http://www.socrata.com/" target="_blank">Powered by Tyler Technologies</a></p></div>
      <div><iframe width="950px" title="Washington State Median Income by County" height="808px" src="https://evergreen.data.socrata.com/w/yiqu-vird/s4mb-uumi?cur=rYVjbMrWceN&from=root" frameborder="0"scrolling="no"><a href="https://evergreen.data.socrata.com/dataset/Washington-State-Median-Income-by-County/yiqu-vird" title="Washington State Median Income by County" target="_blank">Washington State Median Income by County</a></iframe><p><a href="http://www.socrata.com/" target="_blank">Powered by Tyler Technologies</a></p></div>
    SNIPPET,
      'https://evergreen.data.socrata.com/w/yiqu-vird/s4mb-uumi?cur=rYVjbMrWceN&from=root',
      'invalidEmbedCodeMessage',
    ];

    yield 'valid iframe starting with w' => [
      <<<SNIPPET
      <div><iframe width="950px" title="Washington State Median Income by County" height="808px" src="https://evergreen.data.socrata.com/w/yiqu-vird/s4mb-uumi?cur=rYVjbMrWceN&from=root" frameborder="0"scrolling="no"><a href="https://evergreen.data.socrata.com/dataset/Washington-State-Median-Income-by-County/yiqu-vird" title="Washington State Median Income by County" target="_blank">Washington State Median Income by County</a></iframe><p><a href="http://www.socrata.com/" target="_blank">Powered by Tyler Technologies</a></p></div>
    SNIPPET,
      'https://evergreen.data.socrata.com/w/yiqu-vird/s4mb-uumi?cur=rYVjbMrWceN&from=root',
      NULL,
    ];

    yield 'valid iframe starting with stories' => [
      <<<SNIPPET
      <div><iframe width="950px" title="Washington State Median Income by County" height="808px" src="https://evergreen.data.socrata.com/stories/yiqu-vird/s4mb-uumi?cur=rYVjbMrWceN&from=root" frameborder="0"scrolling="no"><a href="https://evergreen.data.socrata.com/dataset/Washington-State-Median-Income-by-County/yiqu-vird" title="Washington State Median Income by County" target="_blank">Washington State Median Income by County</a></iframe><p><a href="http://www.socrata.com/" target="_blank">Powered by Tyler Technologies</a></p></div>
    SNIPPET,
      'https://evergreen.data.socrata.com/stories/yiqu-vird/s4mb-uumi?cur=rYVjbMrWceN&from=root',
      NULL,
    ];

    yield 'invalid iframe starting with dataset' => [
      <<<SNIPPET
      <div><iframe width="950px" title="Washington State Median Income by County" height="808px" src="https://evergreen.data.socrata.com/dataset/yiqu-vird/s4mb-uumi?cur=rYVjbMrWceN&from=root" frameborder="0"scrolling="no"><a href="https://evergreen.data.socrata.com/dataset/Washington-State-Median-Income-by-County/yiqu-vird" title="Washington State Median Income by County" target="_blank">Washington State Median Income by County</a></iframe><p><a href="http://www.socrata.com/" target="_blank">Powered by Tyler Technologies</a></p></div>
    SNIPPET,
      'https://evergreen.data.socrata.com/dataset/yiqu-vird/s4mb-uumi?cur=rYVjbMrWceN&from=root',
      'invalidEmbedCodeMessage',
    ];

    yield 'valid iframe on invalid host' => [
      <<<SNIPPET
      <div><iframe width="950px" title="Washington State Median Income by County" height="808px" src="https://invalid-example.org:8080/stories/yiqu-vird/s4mb-uumi?cur=rYVjbMrWceN&from=root" frameborder="0"scrolling="no"><a href="https://invalid-example.org:8080/dataset/Washington-State-Median-Income-by-County/yiqu-vird" title="Washington State Median Income by County" target="_blank">Washington State Median Income by County</a></iframe><p><a href="http://www.socrata.com/" target="_blank">Powered by Tyler Technologies</a></p></div>
    SNIPPET,
      'https://invalid-example.org:8080/stories/yiqu-vird/s4mb-uumi?cur=rYVjbMrWceN&from=root',
      'invalidHostMessage',
    ];

  }

}
