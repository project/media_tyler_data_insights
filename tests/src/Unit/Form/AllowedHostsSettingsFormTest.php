<?php

declare(strict_types=1);

namespace Drupal\Tests\media_tyler_data_insights\Unit\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\media_tyler_data_insights\Form\AllowedHostsSettingsForm;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the allowed settings form validation logic.
 *
 * @group media_tyler_data_insights
 */
class AllowedHostsSettingsFormTest extends UnitTestCase {

  /**
   * The form object under test.
   *
   * @var \Drupal\media_tyler_data_insights\Form\AllowedHostsSettingsForm
   */
  protected AllowedHostsSettingsForm $sut;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->configFactory = $this->createMock(ConfigFactoryInterface::class);
    $this->sut = new AllowedHostsSettingsForm($this->configFactory);
    $this->sut->setStringTranslation($this->getStringTranslationStub());
  }

  /**
   * Tests the validator works for different snippets as expected.
   *
   * @dataProvider validateDataProvider
   */
  public function testValidateDomain(string $domain, ?string $errorMessage) {
    $result = $this->sut->validateDomain($domain);
    $this->assertEquals($result?->getUntranslatedString(), $errorMessage);
  }

  /**
   * Data provider for :testValidateDomain.
   */
  public function validateDataProvider(): \Generator {
    yield 'random string' => [
      'This is my value',
      'Domain %domain must start with https://',
    ];
    yield 'localhost domain' => [
      'https://localhost',
      NULL,
    ];
    yield 'http only' => [
      'http://example.org',
      'Domain %domain must start with https://',
    ];
    yield 'no scheme' => [
      'example.org/',
      'Domain %domain must start with https://',
    ];
    yield 'localhost with port' => [
      'https://localhost:8080',
      NULL,
    ];
    yield 'localhost with port and slash' => [
      'https://localhost:8080/',
      NULL,
    ];
    yield 'domain with port' => [
      'https://example.org:8080',
      NULL,
    ];
    yield 'ip with port' => [
      'https://127.0.0.1:8080',
      NULL,
    ];
    yield 'domain with path' => [
      'https://example.org/datapath',
      'Please include only the domain. Instead of %domain, maybe you should use %correctedDomain',
    ];

  }

}
