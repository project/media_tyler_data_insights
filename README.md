## INTRODUCTION

This module allows the creation of Media assets from Tyler Technologies Data & Insights. This allows you to manage data and create visualizations within Tyler Technologies Data & Insights and then embed them within Drupal anywhere Media allows, including fields and WYSIWYG.

## REQUIREMENTS

Works with Drupal 9 and 10.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION
- Create a new Media Type and select "Tyler Data & Insights".
- Add the "Administer Media: Tyler Data & Insights allowed hosts" permission to any privileged user that can add hosts.
- Add the permissions for which roles can create media with this new media type.
- Go to <em>Configuration > Media > Tyler Technologies Data & Insights settings</em> and add the hosts that you want to embed from.

## MAINTAINERS

Current maintainers for Drupal 9/10:

- Christian López Espínola (penyaskito) - https://www.drupal.org/u/penyaskito
- Javier Reartes (javi-er) - https://www.drupal.org/u/javi-er
- Andrew Berry (deviantintegral) - https://www.drupal.org/u/deviantintegral

