<?php

declare(strict_types = 1);

namespace Drupal\media_tyler_data_insights\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Configure Media: Tyler Technologies Data & Insights settings for this site.
 */
final class AllowedHostsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'media_tyler_data_insights_allowed_hosts_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['media_tyler_data_insights.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $allowedHosts = $this->config('media_tyler_data_insights.settings')->get('allowed_hosts');
    $form['allowed_hosts'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Allowed Hosts'),
      '#description' => $this->t('List all the allowed hosts for this site visualizations, one host per line.'),
      '#default_value' => $this->getAllowedHostsAsText($allowedHosts),
    ];
    if (\Drupal::moduleHandler()->isLoaded('csp')) {
      $form['allowed_hosts']['#description'] .= ' ' . $this->t('If the <a href="https://www.drupal.org/project/csp" target="_blank">CSP module</a> is enabled, these hosts will be automatically added to the <em>frame-src</em> directive.');
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $allowedHosts = $form_state->getValue('allowed_hosts');
    $allowedHostsList = $this->getAllowedHostsList($allowedHosts);
    $errors = [];
    foreach ($allowedHostsList as $domain) {
      $error = $this->validateDomain($domain);
      if ($error) {
        $errors[] = $error;
      }
    }
    if (count($errors) > 0) {
      $form_state->setErrorByName('allowed_hosts', [
        '#theme' => 'item_list',
        '#list_type' => 'ul',
        '#items' => $errors,
      ]);
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * Validates that the domain is correct.
   *
   * A domain will be correct if:
   * - Is a valid domain with scheme.
   * - The scheme is https.
   * - There is no path in the url.
   *
   * @param string $domain
   *   The domain to validate.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|null
   *   Returns a error message or NULL if the domain is valid.
   */
  public function validateDomain(string $domain): ?TranslatableMarkup {
    $domainInfo = parse_url($domain);
    if (!isset($domainInfo['scheme']) || $domainInfo['scheme'] !== 'https') {
      return $this->t('Domain %domain must start with https://', ['%domain' => $domain]);
    }
    if (!$domainInfo['host'] && isset($domainInfo['path'])) {
      return $this->t('%domain it is not a valid domain', ['%domain' => $domain]);
    }
    if ($domainInfo['host'] && isset($domainInfo['path']) && $domainInfo['path'] !== '/') {
      return $this->t('Please include only the domain. Instead of %domain, maybe you should use %correctedDomain', [
        '%domain' => $domain,
        '%correctedDomain' => $domainInfo['scheme'] . '://' . $domainInfo['host'] . (isset($domainInfo['port']) ? ':' . $domainInfo['port'] : ''),
      ]);
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $allowedHosts = $form_state->getValue('allowed_hosts');
    $this->config('media_tyler_data_insights.settings')
      ->set('allowed_hosts', $this->getAllowedHostsList($form_state->getValue('allowed_hosts')))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Helper function for transforming from a textarea text value to a list.
   *
   * @param string $allowedHosts
   *   Allowed hosts as a string.
   *
   * @return array
   *   Allowed hosts as an array.
   */
  protected function getAllowedHostsList(string $allowedHosts): array {
    $values = explode("\n", $allowedHosts);
    return array_values(array_filter(array_map('trim', $values)));
  }

  /**
   * Helper function for transforming from a list to a textarea text value.
   *
   * @param ?array $allowedHosts
   *   Allowed hosts as an array.
   *
   * @return array
   *   Allowed hosts as a string.
   */
  protected function getAllowedHostsAsText(?array $allowedHosts): string {
    if ($allowedHosts === NULL) {
      return '';
    }
    return implode("\n", $allowedHosts);
  }

}
