<?php

declare(strict_types=1);

namespace Drupal\media_tyler_data_insights\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\AdminContext;
use Drupal\csp\Event\PolicyAlterEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * If CSP module is enabled, add allowed hosts to frame-src policy.
 */
class AllowedHostsCspEventSubscriber implements EventSubscriberInterface {

  /**
   * The admin context.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected AdminContext $adminContext;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Constructs a new AllowedHostsCspEventSubscriber.
   *
   * @param \Drupal\Core\Routing\AdminContext $admin_context
   *   The current admin context.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(AdminContext $admin_context, ConfigFactoryInterface $config_factory) {
    $this->adminContext = $admin_context;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      'csp.policy_alter' => ['onCspPolicyAlter'],
    ];
  }

  /**
   * Alter CSP policy to allow Tyler Data & Insights embeds from allowed hosts.
   *
   * @param \Drupal\csp\Event\PolicyAlterEvent $alterEvent
   *   The Policy Alter event.
   */
  public function onCspPolicyAlter(PolicyAlterEvent $alterEvent): void {
    // Get Policy.
    $policy = $alterEvent->getPolicy();
    // Check if is Admin Route.
    $is_admin_route = $this->adminContext->isAdminRoute();
    $allowedHosts = $this->configFactory->get('media_tyler_data_insights.settings')->get('allowed_hosts') ?? [];

    // Alter CSP directives with the nonce value for non-admin routes.
    // and when the newrelic extension is loaded.
    if (!$is_admin_route && $policy->hasDirective('frame-src')) {
      $policy->appendDirective('frame-src', implode(' ', $allowedHosts));
    }
    if (!$is_admin_route && !$policy->hasDirective('frame-src')) {
      $policy->setDirective('frame-src', implode(' ', $allowedHosts));
    }

  }

}
