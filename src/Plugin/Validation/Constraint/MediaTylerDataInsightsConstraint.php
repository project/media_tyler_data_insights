<?php

declare(strict_types=1);

namespace Drupal\media_tyler_data_insights\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks if a value represents a valid Tyler Data & Insights embed code.
 *
 * @Constraint(
 *   id = "media_tyler_data_insights",
 *   label = @Translation("Media Tyler Data & Insights embed", context = "Validation"),
 *   type = {"string"}
 * )
 */
class MediaTylerDataInsightsConstraint extends Constraint {

  /**
   * The error message if the embed code does not contain a valid iframe code.
   *
   * @var string
   */
  public $invalidEmbedCodeMessage = 'This does not appear to contain a valid Data & Insights embed. Double check against the code copied from the share or embed dialog in Data & Insights and try again.';

  /**
   * The error message if the embed code does not contain a valid host.
   *
   * @var string
   */
  public $invalidHostMessage = 'The Data & Insights server is not currently allowed to be used for embeds on this site. The embed must be hosted at one of %allowed_hosts';

}
