<?php

declare(strict_types=1);

namespace Drupal\media_tyler_data_insights\Plugin\Validation\Constraint;

use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates Tyler Data & Insights embed code.
 */
class MediaTylerDataInsightsConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * A config factory for retrieving required config objects.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  protected $configFactory;

  /**
   * Constructs a new CommentNameConstraintValidator.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A config factory for retrieving required config objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('config.factory'));
  }

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    $allowedHosts = $this->configFactory->get('media_tyler_data_insights.settings')->get('allowed_hosts') ?? [];
    /** @var \Drupal\media\MediaInterface $media */
    $media = $value->getEntity();
    $source = $media->getSource();

    $contents = $source->getSourceFieldValue($media);
    $dom = Html::load($contents);
    $xpath = new \DOMXPath($dom);

    $iframeSrcSelector = $xpath->query('//iframe["src"]');
    if ($iframeSrcSelector->count() !== 1) {
      $this->context->addViolation($constraint->invalidEmbedCodeMessage);
      return;
    }
    $url = $iframeSrcSelector->item(0)->getAttribute('src');
    $urlParts = parse_url($url);
    $pattern = '/^\/(w|stories)\//';
    if (!isset($urlParts['path']) || !preg_match($pattern, $urlParts['path'])) {
      $this->context->addViolation($constraint->invalidEmbedCodeMessage);
      return;
    }
    $allowedHosts = array_map(fn($host) => str_replace('https://', '', $host), $allowedHosts);
    if (!in_array($urlParts['host'], $allowedHosts)) {
      $this->context->addViolation($constraint->invalidHostMessage, ['%allowed_hosts' => implode(',', $allowedHosts)]);
      return;
    }
  }

}
