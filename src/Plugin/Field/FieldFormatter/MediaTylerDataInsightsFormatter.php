<?php

declare(strict_types=1);

namespace Drupal\media_tyler_data_insights\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'Tyler Data & Insights embed' formatter.
 *
 * @FieldFormatter(
 *   id = "media_tyler_data_insights",
 *   label = @Translation("Tyler Data & Insights embed"),
 *   field_types = {
 *     "string_long"
 *   }
 * )
 */
class MediaTylerDataInsightsFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'width' => '100%',
      'height' => '400px',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return parent::settingsForm($form, $form_state) + [
      'width' => [
        '#type' => 'textfield',
        '#title' => $this->t('Width'),
        '#default_value' => $this->getSetting('width'),
        '#size' => 5,
        '#maxlength' => 10,
        '#description' => $this->t('Valid css unit e.g. 500px, 100%, etc.'),
      ],
      'height' => [
        '#type' => 'textfield',
        '#title' => $this->t('Height'),
        '#default_value' => $this->getSetting('height'),
        '#size' => 5,
        '#maxlength' => 10,
        '#description' => $this->t('Valid css unit e.g. 500px, 100%, etc.'),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $summary[] = $this->t('Iframe size: %width , %height.', [
      '%width' => $this->getSetting('width'),
      '%height' => $this->getSetting('height'),
    ]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $title = $item->getEntity()->label();
      $dom = Html::load($item->value);
      $xpath = new \DOMXPath($dom);

      $iframeSrcSelector = $xpath->query('//iframe["src"]');
      $url = $iframeSrcSelector->item(0)->getAttribute('src');

      $element[$delta] = [
        '#theme' => 'media_tyler_data_insights',
        '#url' => $url,
        '#title' => $title,
        '#width' => $this->getSetting('width'),
        '#height' => $this->getSetting('height'),
      ];
    }

    return $element;
  }

}
