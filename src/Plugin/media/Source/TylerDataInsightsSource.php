<?php

declare(strict_types=1);

namespace Drupal\media_tyler_data_insights\Plugin\media\Source;

use Drupal\media\MediaSourceBase;
use Drupal\media\MediaSourceFieldConstraintsInterface;
use Drupal\media\MediaTypeInterface;

/**
 * A media source plugin for Tyler Data & Insights media embeds.
 *
 * @MediaSource(
 *   id = "media_tylerdi",
 *   label = @Translation("Tyler Data & Insights"),
 *   description = @Translation("A media source plugin for Tyler Data & Insights."),
 *   allowed_field_types = {"string_long"},
 *   default_thumbnail_filename = "generic.png",
 *   forms = {
 *     "media_library_add" = "Drupal\media_tyler_data_insights\Form\MediaForm"
 *   }
 * )
 */
class TylerDataInsightsSource extends MediaSourceBase implements MediaSourceFieldConstraintsInterface {

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceFieldConstraints() {
    return [
      'media_tyler_data_insights' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function createSourceField(MediaTypeInterface $type) {
    $field = parent::createSourceField($type);
    $field->set('description', $this->t('Copy and paste the code fragment from the share or embed dialog in Data & Insights.'));
    return $field;
  }

}
